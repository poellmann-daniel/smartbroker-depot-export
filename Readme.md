# Smartbroker CSV Export

Automatically export your main smartbroker depot as CSV with python and selenium.

## Installation and Usage
* Install selenium (```requirements.txt```)
* Copy ```config_template.py``` to ```config.py``` and fill in your username/password.
* Run ```python main.py```
* The CSV of your *main* depot should now be in the ```download``` folder. 

If their website changes too much (-> id/name/class attributes of HTML elements change) it should break. I'll try to fix it on time though.

## Limitations and TODOs
* Does not export your checking account history. Maybe I'll implement that in the future?

## Licence
You can run, modify, redistribute, change the source however you want IF (and only if) you read through the source code. 
This software needs your access credentials for your brokerage account. While it can't steal your funds without your 
TAN generator, it is still important that you check for malicious behaviour 
(there is none, but it's literally just a few lines of python code, so read it!).