import time
import datetime
import shutil
import os

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service

from config import *

download_folder = os.path.abspath(os.getcwd()) + "/download"

options = webdriver.ChromeOptions()
preferences = {"download.default_directory": download_folder}
options.add_experimental_option("prefs", preferences)
options.add_argument("--disable-gpu")
options.add_argument("--disable-browser-side-navigation")
options.add_argument("--disable-dev-shm-usage")
options.add_argument("--disable-infobars")
options.add_argument("--no-sandbox")
# options.add_argument("--headless")


s = Service()
driver = webdriver.Chrome(service=s, options=options)
driver.maximize_window()


def export_portfolio(zugangsnummer, identifier, depot_number, download_folder):
    driver.get(login_page)

    zugangsnummer_element = driver.find_element(By.ID, "zugangsnummer")
    zugangsnummer_element.send_keys(str(zugangsnummer))
    time.sleep(2)

    identifier_element = driver.find_element(By.ID, "identifier")
    identifier_element.send_keys(str(identifier))
    time.sleep(2)

    identifier_element.send_keys(Keys.RETURN)

    driver.get(depot_page)
    time.sleep(2)

    download_elements = driver.find_element(By.CLASS_NAME, "link_csv_export")
    download_elements.click()
    time.sleep(2)

    # rename last downloaded file
    new_name = download_folder + "/" + str(datetime.datetime.now()) + "_" + depot_number + ".csv"
    filename = max([download_folder + "/" + f for f in os.listdir(download_folder)], key=os.path.getctime)
    shutil.move(filename, os.path.join(download_folder, new_name))

    driver.close()


if __name__ == '__main__':
    export_portfolio(zugangsnummer, identifier, depot_number, download_folder)
